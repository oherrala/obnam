use crate::config::ClientConfig;
use crate::error::ObnamError;
use crate::passwords::{passwords_filename, Passwords};
use structopt::StructOpt;

const PROMPT: &str = "Obnam passphrase: ";

#[derive(Debug, StructOpt)]
pub struct Init {
    #[structopt(long)]
    insecure_passphrase: Option<String>,
}

impl Init {
    pub fn run(&self, config: &ClientConfig) -> Result<(), ObnamError> {
        let passphrase = match &self.insecure_passphrase {
            Some(x) => x.to_string(),
            None => rpassword::read_password_from_tty(Some(PROMPT)).unwrap(),
        };

        let passwords = Passwords::new(&passphrase);
        let filename = passwords_filename(&config.filename);
        passwords
            .save(&filename)
            .map_err(|err| ObnamError::PasswordSave(filename, err))?;
        Ok(())
    }
}
