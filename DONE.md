---
title: Definition of done
...

# Definition of done

For changes to this project to be considered done, the following must
be true:

* any changes for new functionality add tests for the functionality
* changes are merged to the main branch
* the automated tests, as invoked by ./check, pass successfully
* CI successfully builds a new .deb package
